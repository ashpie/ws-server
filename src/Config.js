const fs = require('fs');
const Application = require('./Application');

class Config {
    constructor(data) {
        this.wsPort = data.wsPort ? data.wsPort : 6001;
        this.broadcastPort = data.broadcastPort ? data.broadcastPort : 6002;
        this.keyFile = data.keyFile ? data.keyFile : 'server-key.pem';
        this.certFile = data.certFile ? data.certFile : 'server-cert.pem';
        this.apps = [];

        if (typeof data.apps !== 'undefined' && typeof data.apps.length === 'number' && data.apps.length > 0) {
            for (let i in data.apps) {
                this.apps.push(new Application(data.apps[i]));
            }
        } else {
            this.apps.push(new Application({}));
        }
    }

    getApplicationByName(name) {
        for (let i in this.apps) {
            let app = this.apps[i];
            if (app.name === name) {
                return app;
            }
        }

        return null;
    }

    save(file) {
        let config = this;
        fs.writeFileSync(file, JSON.stringify(config, function (key, value) {
            if(typeof value !== 'undefined' && typeof value.save === 'function' && value !== config) {
                value = value.save();
            }
            return value;
        }, '\t'));
    }
}

module.exports = Config;