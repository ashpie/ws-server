class ClientConnection {
    constructor(ws, request, config) {
        this.ws = ws;
        this.config = config;
        this.remote = ws._socket.remoteAddress;
        this.requestUrl = request.url;
        this.args = this.requestUrl.split('/').slice(1);
    }

    join(args) {
        if (typeof args === 'undefined') args = this.args;
        if (args.length === 0) return;

        switch (args[0]) {
            case 'app':
                this.joinApp(args.slice(1));
                break;
            case 'channel':
                this.joinChannel(args.slice(1));
                break;
            default:
                this.ws.send('Malformed request : ' + this.requestUrl);
                this.ws.close();
                return;
        }
    }

    joinApp(args) {
        let requestAppName = args[0];
        let app = this.config.getApplicationByName(requestAppName);
        if (!app) {
            this.error('Unknown app : ' + requestAppName);
            this.ws.close();
            return;
        }

        this.app = app;
        console.log(this.remote + ' fetched ' + this.app.name);
        this.join(args.slice(1));
    }

    joinChannel(args) {
        if (typeof this.app !== 'object') {
            this.error('Can\' connect to a channel without specifying an application.');
            this.ws.close();
            return;
        }

        let requestChannelName = args[0];
        let channel = this.app.getChannelByName(requestChannelName);
        if (!channel) {
            this.error('Unknown channel ' + requestChannelName + ' for app ' + this.app.name + '.');
            this.ws.close();
            return;
        }

        this.channel = channel;
        this.channel.subscribe(this, requestChannelName);
        this.join(args.slice(1));
    }

    send(status, message) {
        let data = {
            status: status,
            message: message
        };

        this.ws.send(JSON.stringify(data));
    }

    error(message) {
        this.send('error', message);
    }

    event(eventName, eventData) {
        let data = {
            status: 'event',
            message: eventName,
            data: eventData,
        };

        this.ws.send(JSON.stringify(data));
    }

    handleMessage(message) {
        let data;
        try {
            data = JSON.parse(message);
        } catch(err) {
            this.error(err);
            return;
        }

        // TODO
        // switch (data.command) {
        //     case 'broadcast':
        //         let broadcastKey = data.broadcastKey;
        //         if(this.app.broadcastKey === broadcastKey) {
        //             this.channel.broadcast(data.name, data.data);
        //         }
        //         break;
        // }
    }

    isOpen() {
        return this.ws.readyState === WebSocket.OPEN;
    }

    close() {
        if (typeof this.channel === 'object') {
            this.channel.unsubscribe(this);
        }
    }
}


module.exports = ClientConnection;