const request = require('request');

class Channel {
    constructor(data) {
        console.log(data);

        this.name = data.name ? data.name : 'example-channel';
        this.fetch = data.fetch ? data.fetch : undefined;
        this.subscriptions = [];

        this.loaded = false;

        this.loadNames();
    }

    async loadNames() {
        console.log('Loading names ...')
        this.names = [];
        if (typeof this.fetch !== 'undefined') {
            for (let i = 0; i < this.fetch.length; i++) {
                let url = this.fetch[i];
                console.log('Fetching ', url);
                try {
                    let raw = await new Promise((resolve, reject) => {
                        request(url, (error, response, body) => {
                            if (error) reject(error);
                            if (response.statusCode != 200) {
                                reject('Request failed. Response code: ' + response.statusCode);
                            }
                            resolve(body);
                        });
                    });
                    let data = JSON.parse(raw);
                    if (typeof data === 'object') {
                        for (let i = 0; i < data.length; i++) {
                            let name = this.name.replace('*', data[i]);
                            this.names.push(name);
                            console.log('Added ', name);
                        }
                    } else {
                        console.error('Channel name fetch got wrong type of json data. Expected an array, got ', data);
                    }
                } catch (error) {
                    console.error('An error occured while fetching', url, ':\n', error);
                }
            }
        } else {
            this.names.push(this.name);
            console.log('Added ', this.name);
        }
        console.log('Loaded');
        this.loaded = true;
    }

    isLoaded() {
        return this.loaded;
    }

    matchesName(name) {
        for (let i = 0; i < this.names.length; i++) {
            if (name === this.names[i]) {
                return true;
            }
        }
        return false;
    }

    save() {
        return {
            name: this.name,
            fetch: this.fetch,
        };
    }

    broadcast(data, channelName) {
        console.log('Broadcasting', data, 'on channel', channelName);
        for (let i in this.subscriptions) {
            let subscription = this.subscriptions[i];
            if (subscription.channelName === channelName) {
                subscription.connection.event("broadcast", data);
            }
        }
    }

    subscribe(connection, channelName) {
        this.subscriptions.push({
            connection: connection,
            channelName: channelName,
        });
        console.log(connection.remote + ' subscribed to ' + connection.requestUrl);
    }

    unsubscribe(connection) {
        this.subscriptions = this.subscriptions.filter(subscription => subscription.connection !== connection);
    }
}

module.exports = Channel;