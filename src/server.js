const WebSocket = require('ws');
const tls = require('tls');
const fs = require('fs');
const Config = require('./Config');
const ClientConnection = require('./ClientConnection');

// Load configuration
const configLocation = 'config.json';
let configData = {
    channels: [{
        name: 'example',
        broadcastKey: 'secret'
    }]
};

if (fs.existsSync(configLocation)) {
    configData = JSON.parse(fs.readFileSync(configLocation));
}

const config = new Config(configData);
config.save(configLocation);


// Start WebSocket server
const wss = new WebSocket.Server({
    port: config.wsPort,
}, () => {
    console.log('Websocket server listening on :' + config.wsPort);
});

wss.on('connection', function (ws, request) {
    let connection = new ClientConnection(ws, request, config);

    ws.on('message', function (msg) {
        connection.handleMessage(msg);
    });

    ws.on('close', function () {
        connection.close();
    });

    connection.join();
});



const authorizedCertificates = [];
for (let i = 0; i < config.apps.length; i++) {
    let app = config.apps[i];
    authorizedCertificates.push(fs.readFileSync(app.broadcastKey));
}

const broadcastServerOptions = {
    key: fs.readFileSync(config.keyFile),
    cert: fs.readFileSync(config.certFile),
    requestCert: true,
    rejectUnauthorized: true,
    ca: authorizedCertificates,
};

// Start broadcasting server
let server = tls.createServer(broadcastServerOptions, function (socket) {
    var prefix = '-----BEGIN CERTIFICATE-----\n';
    var postfix = '-----END CERTIFICATE-----';
    var pemText = prefix + socket.getPeerCertificate().raw.toString('base64').match(/.{0,64}/g).join('\n') + postfix + '\n';

    let app = null;
    for (let i = 0; i < config.apps.length; i++) {
        if (pemText.localeCompare(authorizedCertificates[i]) === 0) {
            app = config.apps[i];
            break;
        }
    }

    if (!app) {
        socket.write('An error occurred while matching certificates. (internal error)\r\n');
        socket.end();
        return;
    }

    socket.setTimeout(2000);
    socket.on('timeout', () => {
        socket.write('Timeout\r\n');
        socket.end();
    });

    socket.write('WS-Server broadcast\r\n');
    let rawCommand = '';
    socket.on('data', data => {
        rawCommand += data;
        try {
            let command = JSON.parse(rawCommand);
            if (!command.channel || !command.data) {
                socket.write('Missing fields. Usage: {channel:"<channelName>", data:{...}}\r\n');
            } else {
                let channel = null;
                for (let i = 0; i < app.channels.length; i++) {
                    if (app.channels[i].matchesName(command.channel)) {
                        channel = app.channels[i];
                        break;
                    }
                }
                if (channel == null) {
                    socket.write('Channel not found "' + data.channel + '".\r\n')
                } else {
                    channel.broadcast(command.data, command.channel);
                    socket.write('Success\r\n');
                }
            }
            socket.end();
        } catch (err) {
            socket.write('Incomplete/Invalid command, waiting for more data.\r\n');
            socket.write('Current command: ' + rawCommand + '\r\n');
            socket.write('Error message: ' + err + '\r\n');
        }
    });
});

tls.createSecureContext();

server.listen(config.broadcastPort, '127.0.0.1', () => {
    console.log('Broadcast server listening on :' + config.broadcastPort);
});