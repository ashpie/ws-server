const Channel = require('./Channel');

class Application {
    constructor(data) {
        this.name = data.name ? data.name : 'example-app';
        this.broadcastKey = data.broadcastKey ? data.broadcastKey : 'client-cert.pem';
        this.channels = [];

        if (typeof data.channels !== 'undefined' && typeof data.channels.length === 'number' && data.channels.length > 0) {
            for (let i in data.channels) {
                this.channels.push(new Channel(data.channels[i]));
            }
        } else {
            this.channels.push(new Channel({}));
        }
    }

    getChannelByName(name) {
        for (let i in this.channels) {
            let channel = this.channels[i];
            if (channel.matchesName(name)) {
                return channel;
            }
        }

        return null;
    }
}

module.exports = Application;